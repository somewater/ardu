#define FIRST_SEGMENT_PIN   2
#define SEGMENT_COUNT       8

byte numberSegments[10] = {
  0b00111111, // 0
  0b00001100, // 1
  0b01011011, // 2
  0b01011110, // 3
  0b01101100, // 4
  0b01110110, // 5
  0b01110111, // 6
  0b00011100, // 7
  0b01111111, // 8
  0b01111110  // 9
};

byte snakeSegments[8] = {
  0b00011000,
  0b00110000,
  0b01100000,
  0b01000100,
  0b00000110,
  0b00000011,
  0b01000001,
  0b01001000
};

void setup()
{
  for (int i = 0; i < SEGMENT_COUNT; ++i)
    pinMode(i + FIRST_SEGMENT_PIN, OUTPUT);
}

void loop()
{
  // определяем число, которое собираемся отображать. Пусть им
  // будет номер текущей секунды, зацикленный на десятке
  int number = (millis() / 100) % 8;
  // получаем код, в котором зашифрована арабская цифра
  int mask = snakeSegments[number];
  // для каждого из 7 сегментов индикатора...
  for (int i = 0; i < SEGMENT_COUNT; ++i) {
    // ...определяем: должен ли он быть включён. Для этого
    // считываем бит (англ. read bit), соответствующий текущему
    // сегменту «i». Истина — он установлен (1), ложь — нет (0)
    boolean enableSegment = bitRead(mask, i);
    if (i == 7) enableSegment = number % 2;
    // включаем/выключаем сегмент на основе полученного значения
    digitalWrite(i + FIRST_SEGMENT_PIN, enableSegment);
  }
}
