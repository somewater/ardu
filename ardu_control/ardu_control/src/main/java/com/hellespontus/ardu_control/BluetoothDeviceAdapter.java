package com.hellespontus.ardu_control;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class BluetoothDeviceAdapter extends ArrayAdapter<BluetoothDevice> {
    public BluetoothDeviceAdapter(Context c, ArrayList<BluetoothDevice> array) {
        super(c, 0, array);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BluetoothDevice device = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.bluetooth_device_list_item, parent, false);
        }

        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView macAddress = (TextView) convertView.findViewById(R.id.mac_address);

        title.setText(device.getName());
        macAddress.setText(device.getAddress());

        return convertView;
    }
}
