package com.hellespontus.ardu_control;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import com.hellespontus.ardu_control.threads.ArduExecutors;
import com.hellespontus.ardu_control.threads.ConnectThread;
import com.hellespontus.ardu_control.threads.ConnectedThread;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class BluetoothService {

    public static final int MESSAGE_CONNECTED = 1;
    public static final int MESSAGE_DISCONNECTED = 2;
    public static final int MESSAGE_DATA = 3;

    private final Handler handler;
    private ConnectThread connectThread;
    private ConnectedThread connectedThread;
    private BluetoothDevice device;
    private volatile boolean restartScheduled = false;
    private ScheduledFuture<?> restartScheduledFuture;

    public BluetoothService(Handler handler) {
        this.handler = handler;
    }

    public void start(BluetoothDevice device) {
        this.device = device;
        connectThread = new ConnectThread(device, this);
        connectThread.start();
    }

    public synchronized void restart() {
        if (restartScheduled)
            return;
        if (connectedThread != null) {
            connectedThread.cancel();
            connectedThread = null;
        }
        connectThread = new ConnectThread(device, this);
        connectThread.start();
    }

    public void stop() {
        cancelConnectThread();
        cancelConnectedThread();
    }

    public void connect(BluetoothSocket socket) {
        connectThread = null;
        connectedThread = new ConnectedThread(socket, this);
        connectedThread.start();
        handler.obtainMessage(MESSAGE_CONNECTED).sendToTarget();
    }

    public synchronized void connectFailed(boolean asyncReconnect) {
        if (asyncReconnect) {
            if (!restartScheduled) {
                handler.obtainMessage(MESSAGE_DISCONNECTED).sendToTarget();
                restartScheduledFuture = ArduExecutors.scheduledExecutor.schedule(new Runnable() {
                    @Override
                    public void run() {
                        if (restartScheduledFuture == null)
                            return;
                        restartScheduledFuture = null;
                        restartScheduled = false;
                        restart();
                    }
                }, 1, TimeUnit.SECONDS);
                restartScheduled = true;
            }
        } else {
            handler.obtainMessage(MESSAGE_DISCONNECTED).sendToTarget();
            restart();
        }
    }

    public void onData(byte[] data) {
        handler.obtainMessage(MESSAGE_DATA, data).sendToTarget();
    }

    public void write(byte[] data) {
        if (connectedThread != null)
            connectedThread.write(data);
        // todo: cache data when connectedThread not created
    }

    public BluetoothDevice getDevice() {
        return device;
    }

    private void cancelConnectThread() {
        if (connectThread != null) {
            connectThread.cancel();
            connectThread = null;
        }
    }

    private void cancelConnectedThread() {
        if (connectedThread != null) {
            connectedThread.cancel();
            connectedThread = null;
        }
    }

    private void cancelScheduledRestart() {
        if (restartScheduledFuture != null) {
            restartScheduledFuture.cancel(true);
            restartScheduledFuture = null;
        }
    }
}
