package com.hellespontus.ardu_control.control;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.badlogic.gdx.backends.android.AndroidFragmentApplication;

public class LibgdxFragmentActivity extends FragmentActivity implements AndroidFragmentApplication.Callbacks {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LibgdxFragment fragment = new LibgdxFragment();
        getSupportFragmentManager().beginTransaction().replace(android.R.id.content, fragment).commit();
    }

    @Override
    public void exit() {

    }
}
