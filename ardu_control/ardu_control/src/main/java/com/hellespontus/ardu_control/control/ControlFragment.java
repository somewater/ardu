package com.hellespontus.ardu_control.control;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.hellespontus.ardu_control.IConnectionListener;
import com.hellespontus.ardu_control.R;

import java.util.HashMap;
import java.util.Map;

public class ControlFragment extends Fragment implements IConnectionListener.IReceiver {

    private IConnectionListener connectionListener;
    private byte[] motorMask = new byte[]{0, 0, 0, 0};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.control_fragment, container, false);

        Button forwardLeft = (Button) v.findViewById(R.id.forward_left);
        Button forwardRight = (Button) v.findViewById(R.id.forward_right);
        Button backwardLeft = (Button) v.findViewById(R.id.backward_left);
        Button backwardRight = (Button) v.findViewById(R.id.backward_right);
        final Map<View, ButtonData> buttonToData = new HashMap<>();
        buttonToData.put(forwardLeft, new ButtonData(true, true, 0));
        buttonToData.put(forwardRight, new ButtonData(true, false, 1));
        buttonToData.put(backwardLeft, new ButtonData(false, true, 3));
        buttonToData.put(backwardRight, new ButtonData(false, false, 2));

        View.OnTouchListener arrowButtonListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN)
                    onArrowButtonDown(buttonToData.get(v));
                else if (event.getAction() == MotionEvent.ACTION_UP)
                    onArrowButtonUp(buttonToData.get(v));
                return true;
            }
        };

        forwardLeft.setOnTouchListener(arrowButtonListener);
        forwardRight.setOnTouchListener(arrowButtonListener);
        backwardLeft.setOnTouchListener(arrowButtonListener);
        backwardRight.setOnTouchListener(arrowButtonListener);

        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        connectionListener = (IConnectionListener) activity;
        super.onAttach(activity);
    }

    @Override
    public void onStop() {
        super.onStop();
        connectionListener = null;
    }

    @Override
    public void onData(byte[] data) {

    }

    private void onArrowButtonDown(ButtonData btn) {
        changeMotorMask(btn.index, (byte)5);
    }

    private void onArrowButtonUp(ButtonData btn) {
        changeMotorMask(btn.index, (byte)0);
    }

    private void changeMotorMask(int index, byte newVal) {
        if (motorMask[index] != newVal) {
            motorMask[index] = newVal;
            StringBuilder msg = new StringBuilder();
            for (byte b : motorMask)
                msg.append(Byte.toString(b));
            msg.append('\n');
            connectionListener.sendData(msg.toString().getBytes());
        }
    }

    private static class ButtonData {
        public final boolean forward;
        public final boolean left;
        public final int index;

        public ButtonData(boolean forward, boolean left, int index) {
            this.forward = forward;
            this.left = left;
            this.index = index;
        }
    }
}
