package com.hellespontus.ardu_control.control;

import com.badlogic.gdx.ApplicationListener;
import com.hellespontus.ardu_control.IConnectionListener;

public class LibgdxApp implements ApplicationListener, IConnectionListener.IReceiver {
    @Override
    public void create() {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void render() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public void onData(byte[] data) {

    }
}
