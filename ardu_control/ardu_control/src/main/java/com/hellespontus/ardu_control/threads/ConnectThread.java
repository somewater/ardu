package com.hellespontus.ardu_control.threads;

import android.app.Fragment;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Build;
import android.util.Log;
import com.hellespontus.ardu_control.BluetoothService;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;

public class ConnectThread extends Thread {
    private final BluetoothDevice device;
    private final BluetoothSocket socket;
    private final BluetoothService service;

    public ConnectThread(BluetoothDevice device, BluetoothService service) {
        this.device = device;
        this.service = service;

        BluetoothSocket tmpSocket = null;
        // from official android docs
        /*try {
            tmpSocket = device.createRfcommSocketToServiceRecord(UUID.randomUUID());
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        // from amperka
        try{
            Method m;
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD)
                m = device.getClass().getMethod("createInsecureRfcommSocket", new Class[] {int.class});
            else
                m = device.getClass().getMethod("createRfcommSocket", new Class[] {int.class});
            tmpSocket = (BluetoothSocket) m.invoke(device, 1);
        } catch (SecurityException | NoSuchMethodException | IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
            Log.d("BLUETOOTH", e.getMessage());
        }
        socket = tmpSocket;
    }

    @Override
    public void run() {
        try {
            socket.connect();
        } catch (IOException e) {
            e.printStackTrace();
            try {
                socket.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            service.connectFailed(true);
            return;
        }

        service.connect(socket);
    }

    public void cancel() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
