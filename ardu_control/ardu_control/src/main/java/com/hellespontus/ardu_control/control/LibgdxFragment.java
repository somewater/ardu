package com.hellespontus.ardu_control.control;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.badlogic.gdx.backends.android.AndroidFragmentApplication;
import com.hellespontus.ardu_control.IConnectionListener;

public class LibgdxFragment extends AndroidFragmentApplication implements IConnectionListener.IReceiver {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return initializeForView(new LibgdxApp());
    }

    @Override
    public void onData(byte[] data) {
        if (listener != null)
            ((IConnectionListener.IReceiver)listener).onData(data);
    }
}
