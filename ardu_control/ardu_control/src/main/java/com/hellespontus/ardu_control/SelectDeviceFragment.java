package com.hellespontus.ardu_control;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.ArrayList;

public class SelectDeviceFragment extends Fragment implements IConnectionListener.ISender {

    public static final int REQUEST_ENABLE_BT = 1;

    private TextView console;
    private ListView list;
    private ArrayAdapter<BluetoothDevice> listAdapter;
    private BluetoothService service;
    private IConnectionListener connectionListener;
    private boolean consoleAutoScroll = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.select_device_fragment, container, false);
        console = (TextView) v.findViewById(R.id.console);
        console.setMovementMethod(new ScrollingMovementMethod());

        list = (ListView) v.findViewById(android.R.id.list);
        listAdapter = new BluetoothDeviceAdapter(getActivity(), new ArrayList<BluetoothDevice>());
        list.setAdapter(listAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                BluetoothDevice device = listAdapter.getItem(position);
                connectTo(device);
            }
        });

        ToggleButton tb = (ToggleButton) v.findViewById(R.id.auto_scroll_toggle);
        tb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                consoleAutoScroll = isChecked;
            }
        });
        tb.setChecked(consoleAutoScroll);

        searchDevices();
        return v;
    }

    @Override
    public void onAttach(Activity activity) {
        connectionListener = (IConnectionListener) activity;
        super.onAttach(activity);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unregisterReceiver(receiver);
        if (service != null) {
            service.stop();
        }
        connectionListener = null;
    }

    @Override
    public void sendData(byte[] data) {
        service.write(data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_ENABLE_BT:
                    onBlutoothEnabled();
                    return;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void searchDevices() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            log("Bluetooth not provided on this device");
            return;
        }

        if (bluetoothAdapter.isEnabled()) {
            onBlutoothEnabled();
        } else {
            log("Bluetooth enable request");
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
    }

    private void onBlutoothEnabled() {
        log("Bluetooth enabled!");
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter.startDiscovery()) {
            log("Discovery started");
            IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
            getActivity().registerReceiver(receiver, filter);
        } else
            log("Discovery starting error");
    }

    private void connectTo(BluetoothDevice device) {
        BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
        log("Try connect to " + device.getName());

        if (service != null) {
            service.stop();
        }
        service = new BluetoothService(handler);
        service.start(device);
        setDeviceState(service.getDevice(), false);
    }

    private void log(int i) {
        log(getString(i));
    }

    @TargetApi(16)
    private void log(String msg) {
        if (console.getLineCount() > 1000) {
            CharSequence allText = console.getText();
            console.setText(allText.subSequence(allText.toString().indexOf("\n", allText.length() / 2), allText.length()));
        }
        console.append(msg + "\n");

        if (consoleAutoScroll && console.getLayout() != null) {
            final int scrollAmount = console.getLayout().getLineTop(console.getLineCount()) - console.getHeight();
            if (scrollAmount > 0)
                console.scrollTo(0, scrollAmount);
            else
                console.scrollTo(0, 0);
        }
    }

    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                listAdapter.add(device);
            }
        }
    };

    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BluetoothService.MESSAGE_CONNECTED:
                    log("Bluetooth connected!");
                    setDeviceState(service.getDevice(), true);
                    connectionListener.connected();
                    break;
                case BluetoothService.MESSAGE_DISCONNECTED:
                    log("BLUETOOTH DISCONNECTED!!!");
                    setDeviceState(service.getDevice(), false);
                    connectionListener.disconnected();
                    break;
                case BluetoothService.MESSAGE_DATA:
                    byte[] data = (byte[]) msg.obj;
                    log("MSG: " + new String(data));
                    connectionListener.onData(data);
                    break;
                default:
                    log(msg.toString());
            }

        }
    };

    private void setDeviceState(BluetoothDevice device, boolean connected) {
        int pos = listAdapter.getPosition(device);
        if (pos != -1) {
            View deviceItem = list.getChildAt(pos);
            deviceItem.setBackgroundColor(getResources().getColor(connected ? R.color.connected : R.color.disconnected));
        }
    }


}
