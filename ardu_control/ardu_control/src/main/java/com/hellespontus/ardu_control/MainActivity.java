package com.hellespontus.ardu_control;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.badlogic.gdx.backends.android.AndroidFragmentApplication;
import com.hellespontus.ardu_control.control.ControlFragment;
import com.hellespontus.ardu_control.control.LibgdxFragment;


public class MainActivity extends ActionBarActivity implements IConnectionListener, AndroidFragmentApplication.Callbacks {

    private boolean connected = false;
    private static final String CONTROL_FRAGMENT_TAG = "control_fragment";
    private IConnectionListener.ISender sender;
    private IConnectionListener.IReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sender = (ISender) getSupportFragmentManager().findFragmentByTag("select_device_fragment");
    }

    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void connected() {
        if (!connected) {
            ViewGroup holder = (ViewGroup) findViewById(R.id.control_holder);
            Fragment fragment = createControlFragment();
            receiver = (IReceiver) fragment;
            getSupportFragmentManager().beginTransaction().add(holder.getId(), fragment, CONTROL_FRAGMENT_TAG).commit();
            connected = true;
        }
    }

    @Override
    public void disconnected() {
        if (connected) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(CONTROL_FRAGMENT_TAG);
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
            receiver = null;
            connected = false;
        }
    }

    @Override
    public void onData(byte[] data) {
        receiver.onData(data);
    }

    @Override
    public void sendData(byte[] data) {
        sender.sendData(data);
    }

    private Fragment createControlFragment() {
        //return new ControlFragment();
        return new LibgdxFragment();
    }

    @Override
    public void exit() {

    }
}
