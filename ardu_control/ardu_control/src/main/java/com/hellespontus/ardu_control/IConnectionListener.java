package com.hellespontus.ardu_control;

public interface IConnectionListener {
    void connected();
    void disconnected();
    void onData(byte[] data);
    void sendData(byte[] data);

    public interface ISender {
        void sendData(byte[] data);
    }

    public interface IReceiver {
        void onData(byte[] data);
    }
}
