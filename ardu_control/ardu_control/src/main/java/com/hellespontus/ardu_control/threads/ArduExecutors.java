package com.hellespontus.ardu_control.threads;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class ArduExecutors {
    public static ScheduledExecutorService scheduledExecutor = Executors.newScheduledThreadPool(5);
    public static ExecutorService bluetoothThread = Executors.newSingleThreadExecutor();
}
