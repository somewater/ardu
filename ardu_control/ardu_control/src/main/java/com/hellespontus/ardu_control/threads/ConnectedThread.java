package com.hellespontus.ardu_control.threads;

import android.bluetooth.BluetoothSocket;
import com.hellespontus.ardu_control.BluetoothService;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ConnectedThread extends Thread {
    private final int BUF_SIZE = 1024;

    private final BluetoothSocket socket;
    private final InputStream in;
    private final OutputStream out;
    private final BluetoothService service;
    private final byte[] writeBuffer = new byte[BUF_SIZE];
    private int writeBufferPos = 0;

    public ConnectedThread(BluetoothSocket socket, BluetoothService service) {
        this.socket = socket;
        this.service = service;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;

        try {
            tmpIn = socket.getInputStream();
            tmpOut = socket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }

        in = tmpIn;
        out = tmpOut;
    }

    @Override
    public void run() {
        byte[] buffer = new byte[BUF_SIZE];
        int bytes;

        while (true) {
            try {
                bytes = readWithTimeout(buffer);
                if (bytes > 0) {
                    byte[] data = new byte[bytes];
                    System.arraycopy(buffer, 0, data, 0, bytes);
                    service.onData(data);
                }
                if (writeBufferPos > 0) {
                    byte[] data = new byte[writeBufferPos];
                    System.arraycopy(writeBuffer, 0, data, 0, writeBufferPos);
                    out.write(data);
                    writeBufferPos = 0;
                }
            } catch (IOException | InterruptedException | ExecutionException | TimeoutException e) {
                service.connectFailed(false);
                return;
            }
        }
    }

    public void write(byte[] bytes) {
        System.arraycopy(bytes, 0, writeBuffer, writeBufferPos, bytes.length);

        writeBufferPos += bytes.length;
    }

    public void cancel() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int readWithTimeout(final byte[] buffer) throws InterruptedException, ExecutionException, TimeoutException {
        return ArduExecutors.bluetoothThread.submit(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return in.read(buffer);
            }
        }).get(1000, TimeUnit.MILLISECONDS);
    }
}
