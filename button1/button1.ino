#include <EEPROM.h>

#define BUTTON_PIN  3
#define BUTTON2_PIN  4
#define LED_PIN     11
#define MIN_LIGHTNESS 1

boolean buttonWasUp[] = {true, true};
boolean buttonDown[] = {false, false};
boolean ledEnabled = false;  // включен ли свет?
int lightness = MIN_LIGHTNESS;
boolean doubleDown = false;

void setup()
{
  pinMode(LED_PIN, OUTPUT);
  pinMode(BUTTON_PIN, INPUT_PULLUP);
  pinMode(BUTTON2_PIN, INPUT_PULLUP);
  Serial.begin(9600);

  lightness = EEPROM.read(0);
}

void loop()
{
  if (isButtonClicked(BUTTON_PIN, 0))
    switchLed();
  if (isButtonClicked(BUTTON2_PIN, 1) && ledEnabled) {
    lightness *= 2;
    if (lightness > 256)
      lightness = MIN_LIGHTNESS;
    else if (lightness == 256)
      lightness = 255;
    updateLed();
    EEPROM.write(0, lightness);
  }

  /*if (buttonDown[0] && buttonDown[1]) {
    if (!doubleDown) {
      switchLed();
      doubleDown = true;
    }
  } else {
    doubleDown = false;
  }*/
}

boolean isButtonClicked(int buttonPin, int buttonIndex) {
  boolean clicked = false;
  boolean buttonIsUp = digitalRead(buttonPin);

  if (buttonWasUp[buttonIndex] && !buttonIsUp) {
    delay(10);
    buttonIsUp = digitalRead(buttonPin);
    if (!buttonIsUp) {
      buttonDown[buttonIndex] = true;
    }
  } else if (buttonIsUp && buttonDown[buttonIndex]) {
    buttonDown[buttonIndex] = false;
    clicked = true;
  }

  buttonWasUp[buttonIndex] = buttonIsUp;
  return clicked;
}

void switchLed() {
  ledEnabled = !ledEnabled;
  updateLed();
}

void updateLed() {
  analogWrite(LED_PIN, (ledEnabled ? lightness : 0));
  Serial.print("Lightness = ");
  Serial.println(lightness);
}
