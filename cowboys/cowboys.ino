#define BUZZER_PIN   9  // пин с пищалкой
#define PLAYER_COUNT 2   // количество игроков-ковбоев
// вместо перечисления всех пинов по-одному, мы объявляем пару
// списков: один с номерами пинов с кнопками, другой — со
// светодиодами. Списки также называют массивами (англ. array)
int buttonPins[PLAYER_COUNT] = {4, 13};
int ledPins[PLAYER_COUNT] = {5, 11};
int scores[PLAYER_COUNT];

// for melody
const int c = 261;
const int d = 294;
const int e = 329;
const int f = 349;
const int g = 391;
const int gS = 415;
const int a = 440;
const int aS = 455;
const int b = 466;
const int cH = 523;
const int cSH = 554;
const int dH = 587;
const int dSH = 622;
const int eH = 659;
const int fH = 698;
const int fSH = 740;
const int gH = 784;
const int gSH = 830;
const int aH = 880;
int counter;
int ledPin1;
int ledPin2;

void setup()
{
  pinMode(BUZZER_PIN, OUTPUT);
  for (int player = 0; player < PLAYER_COUNT; ++player) {
    // при помощи квадратных скобок получают значение в массиве
    // под указанным в них номером. Нумерация начинается с нуля
    pinMode(ledPins[player], OUTPUT);
    pinMode(buttonPins[player], INPUT_PULLUP);
    scores[player] = 0;
  }
  randomSeed(analogRead(0));
}

void loop()
{
  // даём сигнал «пли!», выждав случайное время от 2 до 7 сек
  unsigned long startMillis = millis();
  int pause = random(2000, 7000);
  while(millis() - startMillis < pause) {
    for (int player = 0; player < PLAYER_COUNT; player++) {
      if (isDown(player)) {
        onFalseStart(player);
        return;
      }
    }
  }
  tone(BUZZER_PIN, 3000, 250); // 3 килогерца, 250 миллисекунд

  for (int player = 0; ; player = (player+1) % PLAYER_COUNT) {
    // если игрок номер «player» нажал кнопку...
    if (isDown(player)) {
      // ...включаем его светодиод и сигнал победы на 1 сек
      onWin(player);
      break; // Есть победитель! Выходим (англ. break) из цикла
    }
  }
}

boolean isDown(int player) {
  return !digitalRead(buttonPins[player]);
}

void onWin(int winner) {
  digitalWrite(ledPins[winner], HIGH);
  for(int i = 1; i <= 10; i++) {
    tone(BUZZER_PIN, 3000 + i * 100, 100);
    delay(100);
  }
  digitalWrite(ledPins[winner], LOW);
  checkFinalWin(winner);
}

void onFalseStart(int looser) {
  int winner = opponent(looser);
  for(int i = 1; i <= 10; i++) {
    digitalWrite(ledPins[winner], i % 2);
    tone(BUZZER_PIN, 3000 - 100 * i, 100);
    delay(100);
  }
  checkFinalWin(winner);
}

int opponent(int player) {
  return player == 0 ? 1 : 0;
}

void checkFinalWin(int winner) {
  scores[winner] += 1;
  if (scores[winner] < 10) return;
  digitalWrite(ledPins[winner], HIGH);

  finalMelody(winner);

  scores[0] = 0;
  scores[1] = 0;
}

void finalMelody(int winner) {
  counter = 0;
  ledPin1 = ledPins[winner];
  ledPin2 = ledPins[opponent(winner)];

  //Play first section
  firstSection();

  //Play second section
  secondSection();

  //Variant 1
  beep(f, 250);
  beep(gS, 500);
  beep(f, 350);
  beep(a, 125);
  beep(cH, 500);
  beep(a, 375);
  beep(cH, 125);
  beep(eH, 650);

  delay(500);

  //Repeat second section
  secondSection();

  //Variant 2
  beep(f, 250);
  beep(gS, 500);
  beep(f, 375);
  beep(cH, 125);
  beep(a, 500);
  beep(f, 375);
  beep(cH, 125);
  beep(a, 650);
}

void beep(int note, int duration)
{
  //Play tone on BUZZER_PIN
  tone(BUZZER_PIN, note, duration);

  //Play different LED depending on value of 'counter'
  if(counter % 2 == 0)
  {
    digitalWrite(ledPin1, HIGH);
    delay(duration);
    digitalWrite(ledPin1, LOW);
  }else
  {
    delay(duration);
  }

  //Stop tone on BUZZER_PIN
  noTone(BUZZER_PIN);

  delay(50);

  //Increment counter
  counter++;
}

void firstSection()
{
  beep(a, 500);
  beep(a, 500);
  beep(a, 500);
  beep(f, 350);
  beep(cH, 150);
  beep(a, 500);
  beep(f, 350);
  beep(cH, 150);
  beep(a, 650);

  delay(500);

  beep(eH, 500);
  beep(eH, 500);
  beep(eH, 500);
  beep(fH, 350);
  beep(cH, 150);
  beep(gS, 500);
  beep(f, 350);
  beep(cH, 150);
  beep(a, 650);

  delay(500);
}

void secondSection()
{
  beep(aH, 500);
  beep(a, 300);
  beep(a, 150);
  beep(aH, 500);
  beep(gSH, 325);
  beep(gH, 175);
  beep(fSH, 125);
  beep(fH, 125);
  beep(fSH, 250);

  delay(325);

  beep(aS, 250);
  beep(dSH, 500);
  beep(dH, 325);
  beep(cSH, 175);
  beep(cH, 125);
  beep(b, 125);
  beep(cH, 250);

  delay(350);
}
