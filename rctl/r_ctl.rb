# encoding: utf-8

require 'serialport'
require 'thread'

class RCtl

  PORT = 'COM4'

  def initialize
    start
  end

  def start
    create_sp
    create_threads
  end

  def read(non_block = false)
    check_alive!
    @from_ard.pop(non_block)
  end

  def write m
    check_alive!
    @to_ard << m
  end
  alias_method :<<, :write

  def stop
    @sp.close if @sp
    @sp = nil

    @write_thread.kill if @write_thread
    @read_thread.kill if @read_thread
    @write_thread = nil
    @read_thread = nil
  end

  def alive?
    @read_thread && @read_thread.alive? && @write_thread && @write_thread.alive? && @sp && !@sp.closed?
  end

  protected

  def check_alive!
    raise "Some thread or stream is stopped" unless alive?
  end

  def create_threads
    @from_ard ||= Queue.new
    @to_ard ||= Queue.new

    @read_thread = Thread.new do
      loop do
        m = @sp.read.strip
        @from_ard << m if m.size > 0
      end
    end

    @write_thread = Thread.new do
      loop do
        cmd = @to_ard.pop
        @sp.write cmd
      end
    end
  end

  def create_sp(port_str = PORT)
    baud_rate = 9600
    data_bits = 8
    stop_bits = 1
    parity = SerialPort::NONE

    @sp = SerialPort.new(port_str, baud_rate, data_bits, stop_bits, parity)
    @sp
  end
end

sp = RCtl.new

#require 'pry'; binding.pry

loop {
  sp.write "\0"
  sp.write "\0"
  sp.write "\0"
  puts sp.read
}
