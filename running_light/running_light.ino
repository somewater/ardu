#include <Arduino.h>
#include <HardwareSerial.h>
#include <CommandCon.h>

#define FIRST_LED_PIN  2
#define LAST_LED_PIN   11

int pin = -1;
int prevPin = -1;
CommandCon conn;

void setup()
{
  for (int pin = FIRST_LED_PIN; pin <= LAST_LED_PIN; ++pin)
    pinMode(pin, OUTPUT);
  conn.start();
  conn.setCmdListener(&onRead);
}

void loop()
{
  unsigned long ms = millis();
  if (pin == -1) {
    int mpin = FIRST_LED_PIN + (ms / 120) % 17;
    if (mpin > LAST_LED_PIN) mpin = 21 - mpin;
    digitalWrite(mpin, HIGH);
    delay(10);
    digitalWrite(mpin, LOW);
    prevPin = mpin;
  } else {
    if (prevPin != -1 && prevPin != pin) {
      digitalWrite(prevPin, LOW);
      prevPin = -1;
    }
    digitalWrite(pin, HIGH);
    prevPin = pin;
  }

  conn.loop();
}

void write(String data) {
  Serial.println(data);
}

void onRead(String cmd, String data) {
  if (cmd == "pin") {
    onPinCmd(constrain(FIRST_LED_PIN + data.toInt(), FIRST_LED_PIN, LAST_LED_PIN));
  }
}

void onPinCmd(int value) {
  pin = value;
}


