#include "bluetooth.h"

#include "Arduino.h"
#include "SoftwareSerial.h"
#include "SimpleTimer.h"

#define NL '\n'
#define NL2 ';'

#define BT_TDX 2
#define BT_RDX 4
#define BT_RATE 9600
#define LED 13

#define MOTOR1 6
#define MOTOR2 9
#define MOTOR3 10
#define MOTOR4 11
#define MOTORS 4


void printCnt();
void readMotorCmdFromStreams();
void processBuffer(String & buffer, void (*cmdHandler)(String &));
void commandHandler(String & command);

SoftwareSerial bluetooth(BT_TDX, BT_RDX); // какой порт поключен к TX и RX соответственно НА bluetooth плате
int cnt = 0;    // счетчик
SimpleTimer cntPrintTimer;

int motorPins[] = {MOTOR1, MOTOR2, MOTOR3, MOTOR4};
int motorVals[MOTORS];
float motorCurVals[MOTORS];

String bluetoothBuffer = "";
String serialBuffer = "";

void setup() {
    Serial.begin(9600);   // инициализация порта
    Serial.println("Type AT commands!");
    pinMode(LED, OUTPUT);

    bluetooth.begin(BT_RATE);
    cntPrintTimer.setInterval(500, printCnt);

    for(int i = 0; i < MOTORS; i++) {
    	pinMode(motorPins[i], OUTPUT);
    	motorVals[i] = 0;
    	motorCurVals[i] = 0.0;
    }
}

void loop() {
	cntPrintTimer.run();
	readMotorCmdFromStreams();



    /*while (bluetooth.available() > 0) {
        Serial.write(bluetooth.read());
    }
    while (Serial.available() > 0){
        //delay(100);
        bluetooth.write(Serial.read());
    }*/
}

void readMotorCmdFromStreams() {
	if (bluetooth.available()) {
		while(bluetooth.available()) {
			bluetoothBuffer += (char)bluetooth.read();
		}
		bluetooth.flush();
	} else if (Serial.available()) {
		while(Serial.available()) {
			serialBuffer += (char)Serial.read();
		}
		Serial.flush();
	}

	if (bluetoothBuffer.indexOf(NL) != -1 || bluetoothBuffer.indexOf(NL2) != -1) {
		processBuffer(bluetoothBuffer, & commandHandler);
	} else if (serialBuffer.indexOf(NL) != -1 || serialBuffer.indexOf(NL2) != -1) {
		processBuffer(serialBuffer, & commandHandler);
	}

	for(int m = 0; m < MOTORS; m++) {
		float cur = motorCurVals[m];
		int val = motorVals[m];
		if (cur > val) {
			motorCurVals[m] = max(cur - 0.5, val);
		}
		analogWrite(motorPins[m], cur);
	}
}

void printCnt() {
    cnt++;
    bluetooth.print("c:");  // выводим надпись
    bluetooth.println(cnt);    // выводим значение счетчика и переводим на новую строку
}

void processBuffer(String & buffer, void (*cmdHandler)(String &)) {
	int len = buffer.length();
	int start = 0;
	for (int i = 0; i < len; i++) {
		if ((buffer[i] == NL || buffer[i] == NL2) && (i - 1) > start) {
			String cmd = buffer.substring(start, i);
			cmdHandler(cmd);
			start = i + 1;
		}
	}

	if (start < len) {
		buffer = buffer.substring(start, len);
	} else {
		buffer = "";
	}
}

bool digitString(String & str) {
	int l = str.length();
	for (int i = 0; i < l; i++)
		if (!isDigit(str[i]))
			return false;
	return true;
}

void motorCommandHandler(String & command) {
	for(int m = 0; m < MOTORS; m++) {
			char c = command[m];
			if (isDigit(c)) {
				int val = map(String(c).toInt(), 0, 9, 0, 255);
				Serial.print("  set m=");Serial.print(m);Serial.print(", val=");Serial.println(val);
				if (val > 0)
					motorCurVals[m] = 255.0;
				motorVals[m] = val;
			}
		}
}

void commandHandler(String & command) {
	Serial.print("Process command: ");
	Serial.println(command);

	if (command.startsWith("m:")) {
		String cmdData = command.substring(2);
		motorCommandHandler(cmdData);
	} else if (command.length() == 4 && digitString(command)) {
		motorCommandHandler(command);
	} else {
		Serial.print("Undefined command");
		Serial.println(command);
	}
}
