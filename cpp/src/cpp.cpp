//============================================================================
// Name        : cpp.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdio.h>
#include <time.h>
#include <cstdlib>
using namespace std;

typedef int Array3[3];

void strcat(char *str);
int strlen(char *str);
char strchr(char *str, int pos);
char *cp_str_arr(char src[], char dest[]);
char *cp_str(char * src, char * dest);

void test_char_copying();
void tag_test();
void struct_unsafe_converting();
void print_2d_array(Array3 array[]);



struct tag {
  char lname[20];
  char fname[20];
  int age;
  float rate;
};

struct point {
  char x;
  char y;
  char z;
  char w;
};

void print_tag(struct tag *val) {
  printf("Last Name: %s, First Name: %s\n", val->lname, (*val).fname);
  cp_str("Xxx", val->lname);
}

int main() {
  Array3 array[] = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}, {10, 11, 12}};
  int (*ptr)[3] = array;
  Array3 *p = (Array3*)malloc(sizeof(Array3[2]));
  for (int x = 0; x < 3; x ++)
    for (int y = 0; y < 3; y ++)
      *(*(p + x) + y) = array[x][y];
  cout << *(p + 8) << endl;
  print_2d_array(array);
  cout << endl;
  print_2d_array(p);
  return 0;
}

void print_2d_array(Array3 array[]) {
  for(int i = 0; i < 3; i++)
      for(int j = 0; j < 3; j++) {
        printf("array[%d][%d] = %d (%d)\n", i, j, array[i][j], *(*(array + i) + j));
      }
}

void struct_unsafe_converting() {
  struct point p;
    p.x = 10;
    p.y = 20;
    p.z = 30;
    p.w = 40;

    int *i = (int*)(void*)&p;
    cout << *i << endl;
    *i = 0x09010507;

    cout << (short)p.x << endl;
    cout << (short)p.y << endl;
    cout << (short)p.z << endl;
    cout << (short)p.w << endl;
}

void tag_test() {
  struct tag person;
  cp_str("Jenson", person.lname);
  cp_str("Ted", person.fname);
  print_tag(&person);
  print_tag(&person);
}

void test_char_copying() {
  char arr[] = "Ted";
  char arr2[10];
  clock_t start = clock();
  for(int i = 0; i < 1000000; i++) {
    cp_str(arr, arr2);
  }
  float end = (float)(clock() - start) /  CLOCKS_PER_SEC;
  printf("time %f\n", end);
  strcat(arr2);
}

char *cp_str(char * src, char * dest) {
  char *p = dest;
  while (*src) {
    *p++ = *src++;
  }
  *p = '\0';
  return dest;
}

char *cp_str_arr(char src[], char dest[]) {
  int i = 0;
  while (src[i]) {
    i[dest] = i[src];
    i++;
  }
  dest[i] = '\0';
  return dest;
}

int strlen(char *str) {
  int i = 0;
  while (*(str++) != '\0'){
    i++;
  }
  return i;
}

void strcat(char *str) {
  while(*str != '\0') {
    cout << *str;
    str++;
  }
}

char strchr(char *str, int pos) {
  while(pos > 0) {
    str++;
    pos--;
  }
  return *str;
}

