/*
 * CommandCon.cpp
 */

#include <Arduino.h>
#include <HardwareSerial.h>
#include "CommandCon.h"

CommandCon::CommandCon() {
  _cmdListener = NULL;
  _msgListener = NULL;
}

CommandCon::~CommandCon() {
  _cmdListener = NULL;
  _msgListener = NULL;
}

void CommandCon::start() {
  Serial.begin(9600);
  Serial.setTimeout(50);
}

void CommandCon::loop() {
  while (Serial.available()) {
    char buffer[32];
    int bytesRead = Serial.readBytesUntil('\n', buffer, 32);
    _onRead(String(buffer).substring(0, bytesRead));
  }
}

void CommandCon::send(String msg) {
  char chars[msg.length() + 1];
  msg.toCharArray(chars, msg.length() + 1);
  _send(chars);
}

void CommandCon::send(char *msg) {
  _send(msg);
}

void CommandCon::send(String cmd, String data) {
  int bufsize = cmd.length() + data.length() + 2;
  int cmdlen = cmd.length() + 1;
  char chars[bufsize];
  cmd.toCharArray(chars, bufsize);
  chars[cmd.length()] = ':';
  data.toCharArray(chars + cmdlen, bufsize - cmdlen);
  _send(chars);
}

void CommandCon::send(char *cmd, char *data) {
  send(String(cmd) + String(":") + String(data));
}

void CommandCon::setMsgListener(msgListener f) {
  _msgListener = f;
}

void CommandCon::setCmdListener(cmdListener f) {
  _cmdListener = f;
}

void CommandCon::_onRead(String msg) {
  if (_msgListener != NULL)
    (*_msgListener)(msg);
  int cmdEnd = msg.indexOf(':');
  if (cmdEnd == -1)
    return;
  String cmd = msg.substring(0, cmdEnd);
  if (_cmdListener != NULL)
    (*_cmdListener)(cmd, msg.substring(cmdEnd + 1));
}

void CommandCon::_send(char *msg) {
  Serial.println(msg);
}
