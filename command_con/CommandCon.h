/*
 * CommandCon.h
 */

#ifndef COMMANDCON_H_
#define COMMANDCON_H_

  #include <Arduino.h>
  #include <HardwareSerial.h>

  typedef void(*cmdListener)(String, String);
  typedef void(*msgListener)(String);

  class CommandCon {
    public:
      CommandCon();
      ~CommandCon();

      void start();
      void loop();

      void send(String msg);
      void send(char *msg);
      void send(String cmd, String data);
      void send(char *cmd, char *data);

      void setMsgListener(msgListener);
      void setCmdListener(cmdListener);
    private:
      void _onRead(String);
      void _send(char *msg);
      msgListener _msgListener;
      cmdListener _cmdListener;
  };

#endif /* COMMANDCON_H_ */
