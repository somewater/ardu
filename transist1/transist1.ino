#include <Arduino.h>
#include <HardwareSerial.h>

#define CONTROL_PIN  9
#define INITIAL_SPEED 0.5
#define MAX_SPEED 10

float brightness = 0.0;
float speed = INITIAL_SPEED;
float speedMul = 1.2;

void setup()
{
  pinMode(CONTROL_PIN, OUTPUT);
}

void loop()
{
  brightness = brightness + speed;
  if (brightness >= 256 || brightness <= 0) {
    speed = -speed * speedMul;
    if (abs(speed) > MAX_SPEED || abs(speed) < INITIAL_SPEED) {
      speedMul = 2.0 - speedMul;
      if (abs(speed) > MAX_SPEED)
        speed = (speed > 0 ? 1 : -1) * MAX_SPEED;
      else
        speed = (speed > 0 ? 1 : -1) * INITIAL_SPEED;
    }
    if(brightness > 256) brightness = 256;
    else if (brightness < 0) brightness = 0;
  }

  analogWrite(CONTROL_PIN, (int)(brightness) % 256);

  delay(1);
}
