
#define BUTTON_PIN 3
#define BUTTON_LED_PIN 4
#define BUZZER_PIN 9
#define LDR_PIN    A0

int btn, btnState, btnCount;
int sensor, sensorState, sensorCount;

void setup() {
  pinMode(BUTTON_PIN, INPUT);
  pinMode(BUTTON_LED_PIN, OUTPUT);
  Serial.begin(9600);
  
  btnState = 0;
  btnCount = 0;
  sensorState = 0;
  sensorCount = 0;
}

int updateToggle(int & curState, int & prevState, int & changeCount, int & switchValue) {
  if (curState != prevState) {
    if (changeCount > 5) {
      prevState = curState;
      if (prevState)
        switchValue = !switchValue;
    } else {
      changeCount++;
    }
  } else {
    changeCount = 0;
  }
  return prevState;
}

void loop() {
  int curButton = !digitalRead(BUTTON_PIN);
  int sensorValue = analogRead(LDR_PIN);
  int curSensor = sensorValue > 750 && sensorValue < 1020;
  
  updateToggle(curButton, btnState, btnCount, btn);
  updateToggle(curSensor, sensorState, sensorCount, sensor);
  
  
  int frequency = map(sensorValue, 0, 1023, 2000, 4500);
  
  if (btn)
    tone(BUZZER_PIN, frequency, 60000);
  else
    noTone(BUZZER_PIN);  
  digitalWrite(BUTTON_LED_PIN, btn);  
     
  return;   
  Serial.print("sensorValue="); Serial.print(sensorValue);
  Serial.print("\curSensor="); Serial.print(curSensor);
  Serial.print("\n");
}
