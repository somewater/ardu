package controllers

import java.io._

import gnu.io.{SerialPortEvent, SerialPortEventListener, SerialPort, CommPortIdentifier}
import org.joda.time.DateTime
import play.api._
import play.api.libs.iteratee.Enumerator
import play.api.mvc._

object Application extends Controller {

  implicit val myCustomCharset = Codec.javaSupported("iso-8859-1")

  lazy val communicator: Communicator = {

    new Communicator
  }

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  def test1(name: String, age: Option[Int]) = Action { implicit req =>
    req
    Ok(
      <div>
        Hello
        <i>
          {name}
        </i> with age <b>{age.getOrElse(10)}. Time {DateTime.now()}, Павел</b>
      </div>
    ).as(HTML).withHeaders("hello" -> "world").withCookies(Cookie("myCookie", "cVal"))
  }

  def write() = Action {
    communicator.puts(0)
    Ok("Writed")
  }

  def read() = Action {
    communicator.puts(0)
    val pot = communicator.gets()
    Ok(s"Value = $pot")
  }

  class Communicator extends SerialPortEventListener {
    val identifier = CommPortIdentifier.getPortIdentifier("COM4")
    if (identifier.isCurrentlyOwned) {
      throw new RuntimeException("Port in use")
    }

    val serialPort = identifier.open("app", 2000) match {
      case sp: SerialPort =>
        sp.setSerialPortParams(9600, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE)
        sp
      case _ =>
        throw new RuntimeException("Not serial port")
    }

    serialPort.disableReceiveTimeout()
    serialPort.enableReceiveThreshold(1)

    serialPort.addEventListener(this)
    serialPort.notifyOnDataAvailable(true)

    val out = new PrintWriter(serialPort.getOutputStream)
    val in = serialPort.getInputStream

    def puts(m: Int): Unit = {
      synchronized {
        out.write(m)
        out.flush()
      }
    }

    def gets(): Int = {
      synchronized {
        while(in.available() <= 0){}
        in.read()
      }
    }

    override def serialEvent(serialPortEvent: SerialPortEvent): Unit = {
      println(s"Serial event, $serialPortEvent")
    }
  }

}