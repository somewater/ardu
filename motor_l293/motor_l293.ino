#include <Arduino.h>
#include <Servo.h>

#define MOTORS 2
#define SHIFT_PINS 1

int DS = 8;     // data
int ST_CP = 12; // latch
int SH_CP = 13; // clock
uint16_t shiftPins = 0;

// {X, y, z} - X - speed pin, GPIO; y, z - direction pins (enabled1/enabled1...) - shift pins
int motorPins[MOTORS][3] = {{3, 0, 1}, {5, 2, 3}};

Servo servo;

void setMotor(int speed, int motorPins[]);
void serialControl();
void predefinedControl();
void predefinedServoControl();

void setup()
{
    for (int m = 0; m < MOTORS; m++) {
        int * pins = motorPins[m];
        pinMode(pins[0], OUTPUT);
    }
    pinMode(DS, OUTPUT);
    pinMode(ST_CP, OUTPUT);
    pinMode(SH_CP, OUTPUT);

    Serial.begin(9600);

    shiftDigitalWrite();

    servo.attach(10);// Servo library disables PWM for 10, 11 pins!
}

void loop()
{
    predefinedControl();
    // serialControl;
    // predefinedServoControl();
}

void serialControl() {
    if (Serial.available() > 0) {
        String cmd = Serial.readStringUntil('\n');
        byte motorNum = String(cmd[0]).toInt();
        int speed = cmd.substring(1).toInt();
        Serial.println();
        Serial.println(cmd);
        Serial.print("Speed ");
        Serial.print(speed);
        Serial.print(", motor ");
        Serial.println(motorNum);
        setMotor(speed, motorPins[motorNum]);
    }
    delay(100);
    if (millis() < 1000) {
        Serial.print("shiftPins ");
        Serial.println(shiftPins);
    }
}

void predefinedControl() {
    for (int motor = 0; motor < 2; ++motor) {
        int * pins = motorPins[motor];
        int start = 0;
        int end = 255;
        int step = 5;
        for(int i = -end; i < end; i += step) {
            setMotor(i, pins);
            delay(100);
        }
        for(int i = end; i >= start; i -= step) {
            setMotor(i, pins);
            delay(100);
        }
    }
}

void predefinedServoControl() {
    for(int i = 0; i <= 180; i++)
    {
        servo.write(i);
        delay(100);
    }
    for(int i = 180; i >= 0; i--)
    {
        servo.write(i);
        delay(100);
    }
}

void setMotor(int speed, int motorPins[])
{
    analogWrite(motorPins[0], abs(speed));
    //shiftDigitalWrite(motorPins[1], !reverse);
    //shiftDigitalWrite(motorPins[2], reverse);
    bitWrite(shiftPins, motorPins[1], !(speed >= 0 ? false : true));
    bitWrite(shiftPins, motorPins[2], (speed >= 0 ? false : true));

    Serial.print("motorPins[1] ");
    Serial.print(motorPins[1]);
    Serial.print(", motorPins[2] ");
    Serial.print(motorPins[2]);
    Serial.print(", reverse ");
    Serial.println(speed < 0);

    shiftDigitalWrite();
}

void shiftDigitalWrite(uint8_t pin, uint8_t val) {
    bitWrite(shiftPins, pin, val);

    Serial.print("pin ");
    Serial.print(pin);
    Serial.print(", val ");
    Serial.print(val);
    Serial.print(", ");

    shiftDigitalWrite();
}

void shiftDigitalWrite() {
    Serial.print("shiftPins ");
    Serial.println(String(shiftPins, BIN));

    digitalWrite(ST_CP, LOW);
    shiftOut(DS, SH_CP, MSBFIRST, shiftPins);
    digitalWrite(ST_CP, HIGH);
}