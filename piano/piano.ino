#include <Arduino.h>

#define BUZZER_PIN    13 // пин с пищалкой (англ. «buzzer»)
#define FIRST_KEY_PIN 5  // первый пин с клавишей (англ. «key»)
#define KEY_COUNT     5  // общее количество клавиш

void setup()
{
  pinMode(BUZZER_PIN, OUTPUT);
}

void loop()
{
  for (int i = 0; i < KEY_COUNT; ++i) {
    int keyPin = i + FIRST_KEY_PIN;

    // считываем значение с кнопки. Возможны всего 2 варианта:
    //  * высокий сигнал, 5 вольт, истина — кнопка отпущена
    //  * низкий сигнал, земля, ложь — кнопка зажата
    boolean keyUp = !digitalRead(keyPin);

    if (!keyUp) {
      int frequency = 1000 + 1000 * ((float)i / 7);

      tone(BUZZER_PIN, frequency, 20);
    }
  }
}
